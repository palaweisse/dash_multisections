# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 16:54:44 2021

@author: a.paladini
"""
import os
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import io
import base64
import pandas as pd

#dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = R'C:\Users\a.paladini\OneDrive - Almaviva SpA\Documenti\progetti\webappstruct\c4i'
os.chdir(dir_path)
######################################################################parsing file
def parse_contents(contents, filename, date):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])

    return html.Div([
        html.H5(filename),
        html.H6(datetime.datetime.fromtimestamp(date)),

        dash_table.DataTable(
            data=df.to_dict('records'),
            columns=[{'name': i, 'id': i} for i in df.columns]
        ),

        html.Hr(),  # horizontal line

        # For debugging, display the raw contents provided by the web browser
        html.Div('Raw Content'),
        html.Pre(contents[0:200] + '...', style={
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-all'
        })
    ])

######################################################################APP LAYOUT
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

app.layout = html.Div([  dbc.Row([html.P(' ')])
                       ##########input data
                       , dbc.Row([ dbc.Col([], width = 1)
                                    , dbc.Col([dcc.Upload(id = "upload-data"
                                               , children=html.Div([ 'Drag and Drop or ',
                                                                     html.A('Select Files') ]
                                                                   , style= {'backgroundColor': 'white'
                                                                             , 'color': 'blue'
                                                                             , 'height': '60px'
                                                                             , 'lineHeight': '60px'
                                                                             , 'borderWidth': '1px'
                                                                             , 'borderColor': 'blue'
                                                                             ,'borderStyle': 'dashed'})
                                               , style={'width': '100%',
                                                        'height': '60px',
                                                        'lineHeight': '60px',
                                                        'borderRadius': '5px',
                                                        'textAlign': 'center',
                                                        'margin': '10px'}
                                               )])
                                    , dbc.Col([], width = 1)
                                   ])  
                        , dbc.Row([html.P(' ')]) 
                        , html.Div(id='output-data-upload')
                        , dbc.Row([html.P(' ')])                     
                     ])
                     
####################FILE LOADING
@app.callback(dash.dependencies.Output('output-data-upload', 'children'),
              dash.dependencies.Input('upload-data', 'contents')
              , prevent_initial_call=True)
def update_output(contents):

    children = []
    
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    df = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
    print(df)
    return df
 
############################################################################################################
######################################################STARTING SERVICE
if __name__ == '__main__':
    app.run_server(debug=True, dev_tools_hot_reload = False)    