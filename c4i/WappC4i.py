# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 12:05:33 2021

@author: a.paladini
"""
import os
import dash
import random
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table
import pandas as pd
from datetime import date
import folium
from folium.plugins import BoatMarker

#dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = R'C:\Users\a.paladini\OneDrive - Almaviva SpA\Documenti\progetti\webappstruct\c4i'
os.chdir(dir_path)

############################DATA LOADING
mov = pd.DataFrame.from_dict({'imolRorIHSNumber':['7311329B',
                                                  '7311330A',
                                                  '7311333C',
                                                  '7311332D']
                               , 'seg': [[[37.5013, 15.0742],[31.205753, 29.924526]],
                            		     [[36.753768, 3.058756],[44.4222, 8.9052]],
                                         [[38.1121, 13.3366],[41.9, 12.4833]],
                            		     [[38.1943, 15.5505],[40.863, 14.2767]]]
                               , 'name': [ 'IL VAGABONDO',
                                           'CLELIA',
                                           'TRANQUILLITY',
                                           'ELENVAR']
                               , 'color': ['red',
                                           'green',
                                           'green',
                                           'yellow']
                               , 'flag': ['Panama', 'Malta', 'Italy', 'Cayman Islands']
                               , 'GroupOwner':['EDT Shipmanagement Ltd','Amiri Yachts'
                                               ,'Tranquillity Partners Sarl','Global Yatirim Holding AS']})

def createMap(mov, itr, flags, names, imos, risks, owners):
    if (itr == 0):
        mov = pd.DataFrame.from_dict({'imolRorIHSNumber':['7311329B',
                                                  '7311330A',
                                                  '7311333C',
                                                  '7311332D']
                               , 'seg': [[[37.5013, 15.0742],[31.205753, 29.924526]],
                            		     [[36.753768, 3.058756],[44.4222, 8.9052]],
                                         [[38.1121, 13.3366],[41.9, 12.4833]],
                            		     [[38.1943, 15.5505],[40.863, 14.2767]]]
                               , 'name': [ 'IL VAGABONDO',
                                           'CLELIA',
                                           'TRANQUILLITY',
                                           'ELENVAR']
                               , 'color': ['red',
                                           'green',
                                           'green',
                                           'yellow']
                               , 'flag': ['Panama', 'Malta', 'Italy', 'Cayman Islands']
                               , 'GroupOwner':['EDT Shipmanagement Ltd','Amiri Yachts'
                                               ,'Tranquillity Partners Sarl','Global Yatirim Holding AS']})
        if((flags is not None) & (flags != [])): mov = mov.loc[mov.flag.isin(flags), : ].reset_index(drop = True)
        if((names is not None) & (names != [])): mov = mov.loc[mov.name.isin(names), : ].reset_index(drop = True)
        if((imos is not None) & (imos != [])): mov = mov.loc[mov.imolRorIHSNumber.isin(imos), : ].reset_index(drop = True)
        if((risks is not None) & (risks != [])): mov = mov.loc[mov.color.isin(risks), : ].reset_index(drop = True)
        if((owners is not None) & (owners != [])): mov = mov.loc[mov.GroupOwner.isin(owners), : ].reset_index(drop = True)
              
        m=folium.Map(location=[41.9, 12.4833]
                     , zoom_start= 5
                     , titles = 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}'
                     )
        for i in mov.index:
            ###tratta
            coord = mov.loc[i, 'seg']
            folium.PolyLine(locations=coord,weight=1, color = 'black', dash_array='10').add_to(m)
            ###nave
            popup = folium.Popup(mov.loc[i, 'name'], min_width=10, max_width=10000)
            BoatMarker(location = (coord[0][0], coord[0][1])
                       , popup=popup
                       , heading=181.257360634
                       ,color = mov.loc[i, 'color']).add_to(m)  
        m.save('map.html')
    
    elif(itr == -1):
        mov = pd.DataFrame.from_dict({'imolRorIHSNumber':['7311329B',
                                                  '7311330A',
                                                  '7311333C',
                                                  '7311332D']
                               , 'seg': [[[37.5013, 15.0742],[31.205753, 29.924526]],
                            		     [[36.753768, 3.058756],[44.4222, 8.9052]],
                                         [[38.1121, 13.3366],[41.9, 12.4833]],
                            		     [[38.1943, 15.5505],[40.863, 14.2767]]]
                               , 'name': [ 'IL VAGABONDO',
                                           'CLELIA',
                                           'TRANQUILLITY',
                                           'ELENVAR']
                               , 'color': ['red',
                                           'green',
                                           'green',
                                           'yellow']
                               , 'flag': ['Panama', 'Malta', 'Italy', 'Cayman Islands']
                               , 'GroupOwner':['EDT Shipmanagement Ltd','Amiri Yachts'
                                               ,'Tranquillity Partners Sarl','Global Yatirim Holding AS']})
        
        m=folium.Map(location=[41.9, 12.4833]
                     , zoom_start= 5
                     , titles = 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}'
                     )
        
        for i in mov.index:
            ###tratta
            coord = mov.loc[i, 'seg']
            folium.PolyLine(locations=coord,weight=1, color = 'black', dash_array='10').add_to(m)
            ###nave
            popup = folium.Popup(mov.loc[i, 'name'], min_width=10, max_width=10000)
            BoatMarker(location = (coord[0][0], coord[0][1])
                       , popup=popup
                       , heading=181.257360634
                       ,color = mov.loc[i, 'color']).add_to(m)  
        m.save('map.html')
        
    else:
        mov = pd.DataFrame.from_dict({'imolRorIHSNumber':['7311329B',
                                                  '7311330A',
                                                  '7311333C',
                                                  '7311332D']
                               , 'seg': [[[37.5013, 15.0742],[31.205753, 29.924526]],
                            		     [[36.753768, 3.058756],[44.4222, 8.9052]],
                                         [[38.1121, 13.3366],[41.9, 12.4833]],
                            		     [[38.1943, 15.5505],[40.863, 14.2767]]]
                               , 'name': [ 'IL VAGABONDO',
                                           'CLELIA',
                                           'TRANQUILLITY',
                                           'ELENVAR']
                               , 'color': ['red',
                                           'green',
                                           'green',
                                           'yellow']
                               , 'flag': ['Panama', 'Malta', 'Italy', 'Cayman Islands']
                               , 'GroupOwner':['EDT Shipmanagement Ltd','Amiri Yachts'
                                               ,'Tranquillity Partners Sarl','Global Yatirim Holding AS']})
        if((flags is not None) & (flags != [])): mov = mov.loc[mov.flag.isin(flags), : ].reset_index(drop = True)
        if((names is not None) & (names != [])): mov = mov.loc[mov.name.isin(names), : ].reset_index(drop = True)
        if((imos is not None) & (imos != [])): mov = mov.loc[mov.imolRorIHSNumber.isin(imos), : ].reset_index(drop = True)
        if((risks is not None) & (risks != [])): mov = mov.loc[mov.color.isin(risks), : ].reset_index(drop = True)
        if((owners is not None) & (owners != [])): mov = mov.loc[mov.GroupOwner.isin(owners), : ].reset_index(drop = True)

        m=folium.Map(location=[41.9, 12.4833]
                     , zoom_start= 5
                     , titles = 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}'
                     )
        
        for i in mov.index:
            ###tratta
            coord = mov.loc[i, 'seg']
            coeff = (coord[1][1] - coord[0][1]) / (coord[1][0] - coord[0][0])
            q = coord[1][1] - coeff * coord[1][0]
            bootCoordX = coord[0][0] + ( coord[1][0] - coord[0][0] ) * random.uniform(0, 1)
            bootCoordY = coeff * bootCoordX + q
            folium.PolyLine(locations=coord, weight=1, color = 'black', dash_array='10').add_to(m)
            ###nave
            popup = folium.Popup(mov.loc[i, 'name'], min_width=10, max_width=10000)
            BoatMarker(location = (bootCoordX, bootCoordY)
                       , popup=popup
                       , heading=181.257360634
                       ,color = mov.loc[i, 'color']).add_to(m)  

        m.save('map.html')
        
    return mov

createMap(mov, 0, None, None, None, None, None)

ins = pd.read_csv('data/ins.csv')
imo = pd.read_excel('data/imo.xlsx')
imo = imo[['CoreShipInd', 'FlagCode', 'FlagName',
       'GroupBeneficialOwner', 'IHSLRorIMOShipNo', 'LegalBeSanctionedCompany',
       'LegalDai', 'LegalEuSanctioned', 'LegalEuSanctionedCompany',
       'LegalFlag', 'LegalFlagDisputed', 'LegalFlagHistory', 'LegalOverall',
       'LegalOwnerOfac', 'LegalOwnership', 'LegalOwnershipFatf',
       'LegalOwnershipHistory', 'LegalPortCall180D', 'LegalPortCall1Y',
       'LegalPortCall3M', 'LegalShip', 'LegalShipNonSdn', 'LegalUnsanctioned',
       'LegalUnsanctionedCompany', 'Operator', 'OperatorCountryOfDomicileName',
       'OperatorCountryOfRegistration', 'RegisteredOwner', 'ShipManager',
       'ShipManagerCountryOfDomicileName', 'ShipName', 'ShipStatus',
       'TechnicalManager']]
############################LAYOUT
sfondoApp = 'lightgray'
button_style = {'background-color': 'white'
                , 'border-color': 'blue'}

tab_style = {'borderBottom': '1px solid ' + 'black'
             , 'padding': '6px'
             , 'fontWeight': 'bold'
             , 'backgroundColor': '#000000'
             , 'textColor': 'black'
             , 'color': 'black'}

tab_selected_style = { 'borderTop': '1px solid ' + 'black'
                        , 'borderBottom': '1px solid ' + 'black'
                        , 'backgroundColor': '#1A1D1D'
                        , 'color': 'black'
                        , 'padding': '6px'
                    }     

############################LAYOUT
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

Layouthome = html.Div([ dbc.Row([html.P(' ')])
                        , dbc.Row([html.P(' ')])
                        ##########riga dedicata ai kpi
                        , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Imbarcazioni controllate: 70766"
                                                        , color="primary", className="mr-1", id = 'kpiHome1', style = {'width': '100%'})
                                                            ])                                             
                                             ], width = 4 )
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Con esito: 55079", color="primary", className="mr-1", id = 'kpiHome2', style = {'width': '100%'})
                                                            ])   
                                             ], width = 2)
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Senza esito: 15687", color="primary", className="mr-1", id = 'kpiHome3', style = {'width': '100%'})
                                                            ])
                                             ], width = 2)
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Reati gravi: 0", color="primary", className="mr-1", id = 'kpiHome4', style = {'width': '100%'})
                                                            ])
                                             ], width = 2)
                                  , dbc.Col([], width = 1)
                                 ], style = {'height': '8vh'})
                       , dbc.Row([html.P(' ')])
                       
                       ###########primo range selector
                       , dbc.Row([ dbc.Col([], width = 1)
                                   , dbc.Col([ dcc.DatePickerSingle(id='from-date-home',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   , dbc.Col([ dcc.DatePickerSingle(id='to-date-home',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   
                                   , dbc.Col([], width = 1)
                                   
                                   , dbc.Col([dbc.Button("Search", color="primary", className="mr-1", id = 'homeSearch1')])
                                ])
                       , dbc.Row([html.P(' ')])
                       
                       ###########prima tabella
                       , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([dash_table.DataTable(id = 'tableHome1'
                                                  , columns = [  {'id': 'Defect_ID', 'name': 'Defect_ID'}     
                                                                , {'id': 'Inspection_ID', 'name': 'Inspection_ID'}
                                                                , {'id': 'Defect_Text', 'name': 'Defect_Text'}
                                                                , {'id': 'Authorisation', 'name': 'Authorisation'}
                                                                , {'id': 'Country', 'name': 'Country'}
                                                                , {'id': 'Expanded_Inspection', 'name': 'Expanded_Inspection'}
                                                                , {'id': 'Flag', 'name': 'Flag'}
                                                                , {'id': 'IHSLR_or_IMO_Ship_No', 'name': 'IHSLR_or_IMO_Ship_No'}
                                                                , {'id': 'Inspection_Date', 'name': 'Inspection_Date'}
                                                                , {'id': 'Inspection_Port_Decode', 'name': 'Inspection_Port_Decode'}
                                                                , {'id': 'Last_Updated', 'name': 'Last_Updated'}
                                                                , {'id': 'Number_Of_Defects', 'name': 'Number_Of_Defects'}
                                                                , {'id': 'Ship_Detained', 'name': 'Ship_Detained'}
                                                                , {'id': 'Ship_Name', 'name': 'Ship_Name'}
                                                                , {'id': 'Ship_Type_Decode', 'name': 'Ship_Type_Decode'}
                                                                , {'id': 'Source', 'name': 'Source'}]
                                                 # , data = df.to_dict('rows')
                                                  , page_size= 15
                                                  , sort_action='native'
                                                  , filter_action="native"
                                                  , style_table={'overflowX': 'auto'}
                                                  , style_cell={#'whiteSpace': 'normal',
                                                                    #'height': 'auto'
                                                                    }
                                                  )
                                    , dbc.Col([], width = 1)
                                             ], width = 10)
                               ])
                       , dbc.Row([html.P(' ')])
                       , dbc.Row([html.P(' ')])
                       , dbc.Row([html.P(' ')])
                       , dbc.Row([html.P(' ')])                                              
                       ])

LayoutSearch = html.Div([ dbc.Row([html.P(' ')])
                    ###########secondo range selector
                      , dbc.Row([ dbc.Col([], width = 1)
                                   , dbc.Col([ dcc.DatePickerSingle(id='from-date-search',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   , dbc.Col([ dcc.DatePickerSingle(id='to-date-search',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   
                                   , dbc.Col([], width = 1)
                                   , dbc.Col([dbc.Button("Search", color="primary", className="mr-1", id = 'homeSearch2')])
                                ])
                       , dbc.Row([html.P(' ')])
                        ###########TABS CON TABELLE
                       , dbc.Row([dbc.Col([], width = 1)
                                  , dbc.Col([ 
                                              dcc.Tabs(id='tabs'
                                                   , parent_className='custom-tabs'
                                                   , className='custom-tabs-container'
                                                   , children=[ #######tabella imbarcazioni
                                                                dcc.Tab(label='Imbarcazioni'
                                                                        , children=[dash_table.DataTable(id = 'tabImbarcazioni'
                                                                                                      , columns = [  {'id': 'CoreShipInd'                       , 'name': 'CoreShipInd'                     }
                                                                                                                    , {'id': 'FlagCode'                          , 'name': 'FlagCode'                        }
                                                                                                                    , {'id': 'FlagName'                          , 'name': 'FlagName'                        }
                                                                                                                    , {'id': 'GroupBeneficialOwner'              , 'name': 'GroupBeneficialOwner'            }
                                                                                                                    , {'id': 'IHSLRorIMOShipNo'                  , 'name': 'IHSLRorIMOShipNo'                }
                                                                                                                    , {'id': 'LegalBeSanctionedCompany'          , 'name': 'LegalBeSanctionedCompany'        }
                                                                                                                    , {'id': 'LegalDai'                          , 'name': 'LegalDai'                        }
                                                                                                                    , {'id': 'LegalEuSanctioned'                 , 'name': 'LegalEuSanctioned'               }
                                                                                                                    , {'id': 'LegalEuSanctionedCompany'          , 'name': 'LegalEuSanctionedCompany'        }
                                                                                                                    , {'id': 'LegalFlag'                         , 'name': 'LegalFlag'                       }
                                                                                                                    , {'id': 'LegalFlagDisputed'                 , 'name': 'LegalFlagDisputed'               }
                                                                                                                    , {'id': 'LegalFlagHistory'                  , 'name': 'LegalFlagHistory'                }
                                                                                                                    , {'id': 'LegalOverall'                      , 'name': 'LegalOverall'                    }
                                                                                                                    , {'id': 'LegalOwnerOfac'                    , 'name': 'LegalOwnerOfac'                  }
                                                                                                                    , {'id': 'LegalOwnership'                    , 'name': 'LegalOwnership'                  }
                                                                                                                    , {'id': 'LegalOwnershipFatf'                , 'name': 'LegalOwnershipFatf'              }
                                                                                                                    , {'id': 'LegalOwnershipHistory'             , 'name': 'LegalOwnershipHistory'           }
                                                                                                                    , {'id': 'LegalPortCall180D'                 , 'name': 'LegalPortCall180D'               }
                                                                                                                    , {'id': 'LegalPortCall1Y'                   , 'name': 'LegalPortCall1Y'                 }
                                                                                                                    , {'id': 'LegalPortCall3M'                   , 'name': 'LegalPortCall3M'                 }
                                                                                                                    , {'id': 'LegalShip'                         , 'name': 'LegalShip'                       }
                                                                                                                    , {'id': 'LegalShipNonSdn'                   , 'name': 'LegalShipNonSdn'                 }
                                                                                                                    , {'id': 'LegalUnsanctioned'                 , 'name': 'LegalUnsanctioned'               }
                                                                                                                    , {'id': 'LegalUnsanctionedCompany'          , 'name': 'LegalUnsanctionedCompany'        }
                                                                                                                    , {'id': 'Operator'                          , 'name': 'Operator'                        }
                                                                                                                    , {'id': 'OperatorCountryOfDomicileName'     , 'name': 'OperatorCountryOfDomicileName'   }
                                                                                                                    , {'id': 'OperatorCountryOfRegistration'     , 'name': 'OperatorCountryOfRegistration'   }
                                                                                                                    , {'id': 'RegisteredOwner'                   , 'name': 'RegisteredOwner'                 }
                                                                                                                    , {'id': 'ShipManager'                       , 'name': 'ShipManager'                     }
                                                                                                                    , {'id': 'ShipManagerCountryOfDomicileName'  , 'name': 'ShipManagerCountryOfDomicileName'}
                                                                                                                    , {'id': 'ShipName'                          , 'name': 'ShipName'                        }
                                                                                                                    , {'id': 'ShipStatus'                        , 'name': 'ShipStatus'                      }
                                                                                                                    , {'id': 'TechnicalManager'                  , 'name': 'TechnicalManager'                }]
                                                                                                      , data = imo.to_dict('rows')
                                                                                                      , page_size= 15
                                                                                                      , sort_action='native'
                                                                                                      , filter_action="native"
                                                                                                      , style_table={'overflowX': 'auto'})
                                                                        ]) 
                                                                #######tabella ispezioni
                                                                , dcc.Tab(label='Ispezioni'
                                                                          , children=[dash_table.DataTable(id = 'tabIspezioni'
                                                                                                      , columns = [{'id': 'Defect_ID', 'name': 'Defect_ID'}     
                                                                                                                    , {'id': 'Inspection_ID', 'name': 'Inspection_ID'}
                                                                                                                    , {'id': 'Defect_Text', 'name': 'Defect_Text'}
                                                                                                                    , {'id': 'Authorisation', 'name': 'Authorisation'}
                                                                                                                    , {'id': 'Country', 'name': 'Country'}
                                                                                                                    , {'id': 'Expanded_Inspection', 'name': 'Expanded_Inspection'}
                                                                                                                    , {'id': 'Flag', 'name': 'Flag'}
                                                                                                                    , {'id': 'IHSLR_or_IMO_Ship_No', 'name': 'IHSLR_or_IMO_Ship_No'}
                                                                                                                    , {'id': 'Inspection_Date', 'name': 'Inspection_Date'}
                                                                                                                    , {'id': 'Inspection_Port_Decode', 'name': 'Inspection_Port_Decode'}
                                                                                                                    , {'id': 'Last_Updated', 'name': 'Last_Updated'}
                                                                                                                    , {'id': 'Number_Of_Defects', 'name': 'Number_Of_Defects'}
                                                                                                                    , {'id': 'Ship_Detained', 'name': 'Ship_Detained'}
                                                                                                                    , {'id': 'Ship_Name', 'name': 'Ship_Name'}
                                                                                                                    , {'id': 'Ship_Type_Decode', 'name': 'Ship_Type_Decode'}
                                                                                                                    , {'id': 'Source', 'name': 'Source'}]
                                                                                                      , data = ins[['Defect_ID', 'Inspection_ID', 'Defect_Text', 'Authorisation', 'Country',
                                                                                                                           'Expanded_Inspection', 'Flag', 'IHSLR_or_IMO_Ship_No',
                                                                                                                           'Inspection_Date', 'Inspection_Port_Decode', 'Last_Updated',
                                                                                                                           'Number_Of_Defects', 'Ship_Detained', 'Ship_Name', 'Ship_Type_Decode',
                                                                                                                           'Source']].to_dict('records')
                                                                                                      , page_size= 15
                                                                                                      , sort_action='native'
                                                                                                      , filter_action="native"
                                                                                                      , style_table={'overflowX': 'auto'})
                                                                        ])
                                                                #######tabella porti
                                                                , dcc.Tab(label='Porto'
                                                                          , children=[dash_table.DataTable(id = 'tabPorto'
                                                                                                      , columns = [{"id": 'città', "name":"città"}
                                                                                                                   , {"id": 'codice porto', "name":"codice porto"}
                                                                                                                   , {"id": 'latitudine', "name":"latitudine"}
                                                                                                                   , {"id": 'longitudine', "name":"longitudine"}
                                                                                                                   , {"id": 'percentuale reati', "name":"percentuale reati"}]
                                                                                                     # , data = df.to_dict('rows')
                                                                                                      , page_size= 15
                                                                                                      , sort_action='native'
                                                                                                      , filter_action="native"
                                                                                                      , style_table={'overflowX': 'auto'})
                                                                                      ])
                                                    ])
                                                ], width = 10)
                                  
                                  , dbc.Col([], width = 1)
                               ])
                       , dbc.Row([html.P(' ')])                       
                       ##########input data
                       , dbc.Row([ ])  
                        , dbc.Row([html.P(' ')]) 
                        , dbc.Row([html.P(' ')])    
                       ])
LayoutRisk = html.Div([  dbc.Row([html.P(' ')])
                        ##########filtri su due colonne
                        , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([ dbc.FormGroup([dbc.Label(" Flag " )
                                                            , dbc.Col( dcc.Dropdown(id = "flagMenu"
                                                                               , options = [ {'label': i, 'value': i}   for i in mov.flag.unique() ]
                                                                               , multi = True)
                                                                      )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("name")
                                                            , dbc.Col( dcc.Dropdown(id = "nameMenu"
                                                                               , options = [ {'label': i, 'value': i}   for i in mov.name.unique() ]
                                                                               , multi = True)
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Imo code",)
                                                            , dbc.Col( dcc.Dropdown(id = "imoMenu"
                                                                               , options = [ {'label': i, 'value': i}   for i in mov.imolRorIHSNumber.unique() ]
                                                                               , multi = True) 
                                                                      )],
                                                            row=True)                                             
                                             ], width = 2)
                                  , dbc.Col([ dbc.FormGroup([dbc.Label("risk level")
                                                            , dbc.Col( dcc.Dropdown(id = "riskMenu"
                                                                               , options = [ {'label': i, 'value': i}   for i in mov.color.unique() ] 
                                                                               , multi = True)
                                                                      )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Owner")
                                                            , dbc.Col( dcc.Dropdown(id = "groupOwnerMenu"
                                                                               , options = [ {'label': i, 'value': i}   for i in mov.GroupOwner.unique() ]
                                                                               , multi = True)
                                                                      )],
                                                            row=True)
                                              , dbc.Button("Search", id = 'searchMap', color="primary", className="mr-1", style = {'align': 'center'}) 
                                             ], width = 2)
                                             
                        ##########rappresentazione grafica a mappa/grafo
                        , dbc.Col([ html.Iframe(id = 'map', srcDoc = open('map.html', 'r').read()
                                                , width = '100%', height = '600' )
                                    , dcc.Interval( id='interval-component'
                                                              , interval= 10 * 1000 #in milliseconds
                                                              , n_intervals=0
                                                            )
                                    , dbc.Button("Refresh", id = 'refreshMap', color="primary", className="mr-1", style = {'align': 'center'})
                                     ]) 
                        , dbc.Col([], width = 1)
                                 ])
                      , dbc.Row([html.P(' ')])    
                      , dbc.Row([ dbc.Col([ ], width = 1)
                                  , dbc.Col([ dash_table.DataTable(id = 'tableRisk'
                                              , columns = [{"id": 'col1', "name":"col1"}
                                                           , {"id": 'col2', "name":"col2"}
                                                           , {"id": 'col3', "name":"col3"}
                                                           , {"id": 'col4', "name":"col4"}
                                                           , {"id": 'col5', "name":"col5"}]
                                             # , data = df.to_dict('rows')
                                              , page_size= 10
                                              , sort_action='native'
                                              , filter_action="native"
                                              ) ])
                                  , dbc.Col([ ], width = 1)            
                                ])
                       , dbc.Row([html.P(' ')]) 
                     ])
LayoutExp = html.Div([ html.P('Report') ])
LayoutAud = html.Div([ html.P('Audit') ])

app.layout = html.Div([ 
                        html.Div([ 
                            #########HEADER AREE DELLA WEBAPP
                            dbc.Row([html.P(' ')])
                            , dbc.Row([  dbc.Col([  ], width = 1)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/home.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='homePage', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/search.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='searchPage', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/risk.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='RiskManagement', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/expSearch.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='ExploratoryAnalysis', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/audit.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='Audit', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([  ], width = 1)
                                     ])
                               , dbc.Row([html.P(' ')])
                                ], style = {'background-color': 'lightgray'})
                        
                        ,html.Div( children = [html.Br()])
                        
                        #########BODY DELLA WEBAPP
                        , html.Div( id = 'body'
                                    , children = [Layouthome], style = {'background-color': 'lightgray'})
                        , html.Div( id = 'bodyHidden'
                                    , children = [LayoutRisk], style = {'background-color': 'lightgray'
                                                                        , 'display': 'none'})
                        ])

############################################################################################################
######################################################CALLBACKS
############################scelta della pagina da aprire
@app.callback(
    [dash.dependencies.Output('body', 'children')],
    [dash.dependencies.Input('homePage', 'n_clicks')
     , dash.dependencies.Input('searchPage', 'n_clicks')
     , dash.dependencies.Input('RiskManagement', 'n_clicks')
     , dash.dependencies.Input('ExploratoryAnalysis', 'n_clicks')
     , dash.dependencies.Input('Audit', 'n_clicks')
     ]
    , prevent_initial_call=True)
def body( n_clicks_home, n_clicks_search, n_clicks_risk, n_clicks_exp, n_clicks_aud ):    
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if( trg == 'homePage' ): 
        rispLay = Layouthome
    elif( trg == 'searchPage' ): 
        rispLay = LayoutSearch
    elif( trg == 'RiskManagement' ): 
        rispLay = LayoutRisk
    elif( trg == 'ExploratoryAnalysis' ): 
        rispLay = LayoutExp
    elif( trg == 'Audit' ): 
        rispLay = LayoutAud
    
    return [rispLay]


############################movimento e filtri nella mappa
@app.callback(
    dash.dependencies.Output('map', 'srcDoc')
    , [ dash.dependencies.Input('interval-component', 'n_intervals')
       , dash.dependencies.Input('searchMap', 'n_clicks')
       , dash.dependencies.Input('refreshMap', 'n_clicks')]
    , [dash.dependencies.State('flagMenu', 'value')
       , dash.dependencies.State('nameMenu', 'value')
       , dash.dependencies.State('imoMenu', 'value')
       , dash.dependencies.State('riskMenu', 'value')
       , dash.dependencies.State('groupOwnerMenu', 'value')]
    , prevent_initial_call=True)
def update_map(n_intervals, n_clicksSearch, n_clicksRefresh, flags, names, imos, risks, owners):
    global mov
    ####per capire chi ha fatto scattare la callback
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if(trg == 'interval-component'):
        mov = createMap(mov, n_intervals, flags, names, imos, risks, owners)
    if(trg == 'refreshMap'): 
        mov = createMap(mov, -1, None, None, None, None, None)
    if(trg == 'searchMap'):
        mov = createMap(mov, 0, flags, names, imos, risks, owners)
        
    return open('map.html', 'r').read()

############################quali ispezioni visualizzare nella tabella home
@app.callback(
    dash.dependencies.Output('tableHome1', 'data'),
    [ dash.dependencies.Input('kpiHome1', 'n_clicks')
     , dash.dependencies.Input('kpiHome2', 'n_clicks')
     , dash.dependencies.Input('kpiHome3', 'n_clicks')
     , dash.dependencies.Input('kpiHome4', 'n_clicks')]
    , prevent_initial_call=True)
def update_tableHome(n_clicks1, n_clicks2, n_clicks3, n_clicks4):   
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if(trg == 'kpiHome1'):
        data = ins[['Defect_ID', 'Inspection_ID', 'Defect_Text', 'Authorisation', 'Country',
                       'Expanded_Inspection', 'Flag', 'IHSLR_or_IMO_Ship_No',
                       'Inspection_Date', 'Inspection_Port_Decode', 'Last_Updated',
                       'Number_Of_Defects', 'Ship_Detained', 'Ship_Name', 'Ship_Type_Decode',
                       'Source']].to_dict('records')
    elif(trg == 'kpiHome2'):
        app = ins.loc[ins.irregolare == 1, : ]
        data = app[['Defect_ID', 'Inspection_ID', 'Defect_Text', 'Authorisation', 'Country',
                       'Expanded_Inspection', 'Flag', 'IHSLR_or_IMO_Ship_No',
                       'Inspection_Date', 'Inspection_Port_Decode', 'Last_Updated',
                       'Number_Of_Defects', 'Ship_Detained', 'Ship_Name', 'Ship_Type_Decode',
                       'Source']].to_dict('records')
    elif(trg == 'kpiHome3'):
        app = ins.loc[ins.irregolare == 0, : ]
        data = app[['Defect_ID', 'Inspection_ID', 'Defect_Text', 'Authorisation', 'Country',
                       'Expanded_Inspection', 'Flag', 'IHSLR_or_IMO_Ship_No',
                       'Inspection_Date', 'Inspection_Port_Decode', 'Last_Updated',
                       'Number_Of_Defects', 'Ship_Detained', 'Ship_Name', 'Ship_Type_Decode',
                       'Source']].to_dict('records')
    elif(trg == 'kpiHome4'):
        data = pd.DataFrame([], columns = ['Defect_ID', 'Inspection_ID', 'Defect_Text', 'Authorisation', 'Country',
                       'Expanded_Inspection', 'Flag', 'IHSLR_or_IMO_Ship_No',
                       'Inspection_Date', 'Inspection_Port_Decode', 'Last_Updated',
                       'Number_Of_Defects', 'Ship_Detained', 'Ship_Name', 'Ship_Type_Decode',
                       'Source']).to_dict('records')
    return data

############################################################################################################
######################################################STARTING SERVICE
if __name__ == '__main__':
    app.run_server(debug=True, dev_tools_hot_reload = False)














