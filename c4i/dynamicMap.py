import os
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table
import pandas as pd
import folium
from folium.plugins import BoatMarker

#dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = R'C:\Users\a.paladini\OneDrive - Almaviva SpA\Documenti\progetti\webappstruct\c4i'
os.chdir(dir_path)

############################DATA MOVEMENT LOADING
mov = pd.DataFrame.from_dict({'imolRorIHSNumber':['7311329B',
                                                  '7311330A',
                                                  '7311333C',
                                                  '7311332D']
                               , 'seg': [[[37.5013, 15.0742],[31.205753, 29.924526]],
                            		     [[36.753768, 3.058756],[44.4222, 8.9052]],
                                         [[38.1121, 13.3366],[41.9, 12.4833]],
                            		     [[38.1943, 15.5505],[40.863, 14.2767]]]
                               , 'name': [ 'Boat1',
                                           'Boat2',
                                           'Boat3',
                                           'Boat4']  })
    
######################################################################MAP OBJECT
####################create empty map
m=folium.Map(location=[41.9, 12.4833]
             , zoom_start= 5
             , titles = 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}'
             )
for i in mov.index:
    ###tratta
    coord = mov.loc[i, 'seg']
    my_PolyLine=folium.PolyLine(locations=coord,weight=1, color = 'black', dash_array='10').add_to(m)
    ###nave
    popup = folium.Popup(mov.loc[i, 'name'], min_width=10, max_width=10000)
    BoatMarker(location = (coord[0][0], coord[0][1])
               , popup=popup
               , heading=181.257360634).add_to(m)

m.save('map.html')

def routes(mov):
    
    return

def boatPosition(inizio, fine, passo):
    boatCoord = [inizio[0] + (fine[0] - inizio[0]) * passo / 10
                 , fine[1] + (fine[1] - inizio[1]) * passo / 10]    
    return boatCoord

######################################################################APP LAYOUT
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

app.layout = html.Div([  dbc.Row([html.P(' ')])
                        ##########filtri su due colonne
                        , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([ dbc.FormGroup([dbc.Label("Field1" )
                                                            , dbc.Col( dbc.Input( id="field1Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field2")
                                                            , dbc.Col( dbc.Input( id="field2Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field3",)
                                                            , dbc.Col( dbc.Input( id="field3Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field7",)
                                                            , dbc.Col( dbc.Input( id="field7Row", placeholder="value"),
                                                                    )],
                                                            row=True)                                             
                                             ], width = 2)
                                  , dbc.Col([ dbc.FormGroup([dbc.Label("Field4")
                                                            , dbc.Col( dbc.Input( id="field4Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field5")
                                                            , dbc.Col( dbc.Input( id="field5Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field6")
                                                            , dbc.Col( dbc.Input( id="field6Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.Button("Search", id = "map_button", color="primary", className="mr-1", style = {'align': 'center'})
                                             ], width = 2)
                                             
                        ##########rappresentazione grafica a mappa/grafo
                        , dbc.Col([ html.Iframe(id = 'map', srcDoc = open('map.html', 'r').read()
                                                , width = '100%', height = '600' )
                                    , dcc.Interval( id='interval-component'
                                                              , interval= 10 * 1000 #in milliseconds
                                                              , n_intervals=0
                                                            )
                                     ]) 
                        , dbc.Col([], width = 1)
                                 ])
                      , dbc.Row([html.P(' ')])    
                      , dbc.Row([ dbc.Col([ ], width = 1)
                                  , dbc.Col([ dash_table.DataTable(id = 'table1'
                                              , columns = [{"id": 'col1', "name":"col1"}
                                                           , {"id": 'col2', "name":"col2"}
                                                           , {"id": 'col3', "name":"col3"}
                                                           , {"id": 'col4', "name":"col4"}
                                                           , {"id": 'col5', "name":"col5"}]
                                             # , data = df.to_dict('rows')
                                              , page_size= 10
                                              , sort_action='native'
                                              , filter_action="native"
                                              ) ])
                                  , dbc.Col([ ], width = 1)            
                                ])
                       , dbc.Row([html.P(' ')]) 
                     ])
                     
                     
    
@app.callback(
    dash.dependencies.Output('map', 'srcDoc'),
    [ dash.dependencies.Input('interval-component', 'n_intervals')]
    , prevent_initial_call=True)
def update_map(n_intervals):
    print(n_intervals)
    ####rotte
    m=folium.Map(location=[41.9, 12.4833]
                 , zoom_start= 5
                 , titles = 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}'
                 )
    m.save('map.html')
    coord = [[41.9, 12.4833],
            [39.2236, 9.1181]]

    my_PolyLine=folium.PolyLine(locations=coord,weight=1, color = 'black', dash_array='10')
    ####navi con pop up
    pp = 'info sulla nave'
    popup = folium.Popup(pp, min_width=10, max_width=10000)
    boatCoord = boatPosition(coord[0], coord[1], n_intervals)
    BoatMarker(location = (boatCoord[0], boatCoord[1])
               , popup=popup
               , heading=181.257360634).add_to(m)

    m.add_child(my_PolyLine)
    m.save('map.html')
    return open('map.html', 'r').read()


############################################################################################################
######################################################STARTING SERVICE
if __name__ == '__main__':
    app.run_server(debug=True, dev_tools_hot_reload = False)    