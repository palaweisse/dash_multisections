# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 12:05:33 2021

@author: a.paladini
"""

import os
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
import dash_table
from datetime import date

#dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = R''
#os.chdir(dir_path)

############################LAYOUT
sfondoApp = 'lightgray'
button_style = {'background-color': 'white'
                , 'border-color': 'blue'}

tab_style = {'borderBottom': '1px solid ' + 'black'
             , 'padding': '6px'
             , 'fontWeight': 'bold'
             , 'backgroundColor': '#000000'
             , 'textColor': 'black'
             , 'color': 'black'}

tab_selected_style = { 'borderTop': '1px solid ' + 'black'
                        , 'borderBottom': '1px solid ' + 'black'
                        , 'backgroundColor': '#1A1D1D'
                        , 'color': 'black'
                        , 'padding': '6px'
                    }     

############################LAYOUT
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

Layouthome = html.Div([ dbc.Row([html.P(' ')])
                        
                        ##########riga dedicata ai kpi
                        , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([ html.P( 'kpi1', style ={'background-color': sfondoApp }) ], width = 2 )
                                  , dbc.Col([], width = 3)
                                  , dbc.Col([ html.P('kpi2') ], width = 2)
                                  , dbc.Col([ html.P('kpi3') ], width = 2)
                                  , dbc.Col([ html.P('kpi4') ], width = 2)
                                 ])
                       , dbc.Row([html.P(' ')])
                       
                       ###########primo range selector
                       , dbc.Row([ dbc.Col([], width = 1)
                                   , dbc.Col([ dcc.DatePickerSingle(id='from-date1',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   , dbc.Col([ dcc.DatePickerSingle(id='to-date1',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   
                                   , dbc.Col([], width = 1)
                                   
                                   , dbc.Col([dbc.Button("Search", color="primary", className="mr-1", id = 'homeSearch1')])
                                ])
                       , dbc.Row([html.P(' ')])
                       
                       ###########prima tabella
                       , dbc.Row([dbc.Col([], width = 1)
                                  , dbc.Col([dash_table.DataTable(id = 'table1'
                                                  , columns = [{"id": 'col1', "name":"col1"}
                                                               , {"id": 'col2', "name":"col2"}
                                                               , {"id": 'col3', "name":"col3"}
                                                               , {"id": 'col4', "name":"col4"}
                                                               , {"id": 'col5', "name":"col5"}]
                                                 # , data = df.to_dict('rows')
                                                  , page_size= 10
                                                  , sort_action='native'
                                                  , filter_action="native"
                                                  )
                                             ], width = 10)
                                  , dbc.Col([], width = 1)
                               ])
                       , dbc.Row([html.P(' ')])
                       
                       ###########secondo range selector
                      , dbc.Row([ dbc.Col([], width = 1)
                                   , dbc.Col([ dcc.DatePickerSingle(id='from-date2',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   , dbc.Col([ dcc.DatePickerSingle(id='to-date2',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   
                                   , dbc.Col([], width = 1)
                                   , dbc.Col([dbc.Button("Search", color="primary", className="mr-1", id = 'homeSearch2')])
                                ])
                       , dbc.Row([html.P(' ')])
                       
                       ###########seconda tabella
                       , dbc.Row([dbc.Col([], width = 1)
                                  , dbc.Col([dash_table.DataTable(id = 'table2'
                                                  , columns = [{"id": 'col1', "name":"col1"}
                                                               , {"id": 'col2', "name":"col2"}
                                                               , {"id": 'col3', "name":"col3"}
                                                               , {"id": 'col4', "name":"col4"}
                                                               , {"id": 'col5', "name":"col5"}]
                                                 # , data = df.to_dict('rows')
                                                  , page_size= 10
                                                  , sort_action='native'
                                                  , filter_action="native"
                                                  )
                                             ], width = 10)
                                  , dbc.Col([], width = 1)
                               ])
                       , dbc.Row([html.P(' ')])
                       ])
                        

LayoutSearch = html.Div([ dbc.Row([html.P(' ')])
                        ##########riga della prima schermata a tabs
                        , dbc.Row([ dbc.Col([], width = 1)
                                    , dbc.Col([
                                        dcc.Tabs(id='tabs'
                                                   , parent_className='custom-tabs'
                                                   , className='custom-tabs-container'
                                                   , children=[
                                                                dcc.Tab(label='Imbarcazioni', children=[html.Div(id='SNA', children = [])
                                                                        ]) 
                                                                , dcc.Tab(label='Ispezioni', children=[html.Div(id='STA', children = [])
                                                                        ])
                                                                , dcc.Tab(label='Bandiera', children=[html.Div(id='COM', children = [])
                                                                        ])
                                                                , dcc.Tab(label='Porto', children=[html.Div(id='COM', children = [])
                                                                        ])
                                                    ])
                                              ])
                                    , dbc.Col([], width = 1)
                                ])
                        , dbc.Row([html.P(' ')])    
                        , dbc.Row([html.P(' ')])    
                        ##########riga della seconda schermata a tabs
                        , dbc.Row([ dbc.Col([], width = 1)
                                    , dbc.Col([
                                        dcc.Tabs(id='tabs2'
                                                   , parent_className='custom-tabs'
                                                   , className='custom-tabs-container'
                                                   , children=[
                                                                dcc.Tab(label='Dettagli', children=[html.Div(id='SNA', children = [])
                                                                        ]) 
                                                                , dcc.Tab(label='Ispezioni', children=[html.Div(id='STA', children = [])
                                                                        ])
                                                                , dcc.Tab(label='Imbarcazioni Transitanti', children=[html.Div(id='COM', children = [])
                                                                        ])
                                                    ])
                                              ])
                                    , dbc.Col([], width = 1)
                                ])
                        , dbc.Row([html.P(' ')])    
                       ])
LayoutRisk = html.Div([  dbc.Row([html.P(' ')])
                        ##########filtri su due colonne
                        , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([ dbc.FormGroup([dbc.Label("Field1" )
                                                            , dbc.Col( dbc.Input( id="field1Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field2")
                                                            , dbc.Col( dbc.Input( id="field2Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field3",)
                                                            , dbc.Col( dbc.Input( id="field3Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field7",)
                                                            , dbc.Col( dbc.Input( id="field7Row", placeholder="value"),
                                                                    )],
                                                            row=True)                                             
                                             ], width = 2)
                                  , dbc.Col([ dbc.FormGroup([dbc.Label("Field4")
                                                            , dbc.Col( dbc.Input( id="field4Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field5")
                                                            , dbc.Col( dbc.Input( id="field5Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.FormGroup([dbc.Label("Field6")
                                                            , dbc.Col( dbc.Input( id="field6Row", placeholder="value"),
                                                                    )],
                                                            row=True)
                                              , dbc.Button("Search", color="primary", className="mr-1", style = {'align': 'center'})                                                            
                                             ], width = 2)
                                             
                        ##########rappresentazione grafica a mappa/grafo
                        , dbc.Col([ dcc.Graph(id="my-graph",
                                                    figure={
                                                        "data": [
                                                            {"x": [1, 2, 3], "y": [4, 1, 2], "type": "bar"},
                                                            {"x": [1, 2, 3], "y": [2, 4, 5], "type": "bar"},
                                                        ],
                                                        "layout": {
                                                            "title": "My Dash Graph",
                                                            "height": 500,
                                                            "width": 800
                                                        },
                                                    },
                                                )
                                     ]) 
                          #, dbc.Col([], width = 1)
                                 ])
                      , dbc.Row([html.P(' ')])    
                      , dbc.Row([ dbc.Col([ ], width = 1)
                                  , dbc.Col([ dash_table.DataTable(id = 'table1'
                                              , columns = [{"id": 'col1', "name":"col1"}
                                                           , {"id": 'col2', "name":"col2"}
                                                           , {"id": 'col3', "name":"col3"}
                                                           , {"id": 'col4', "name":"col4"}
                                                           , {"id": 'col5', "name":"col5"}]
                                             # , data = df.to_dict('rows')
                                              , page_size= 10
                                              , sort_action='native'
                                              , filter_action="native"
                                              ) ])
                                  , dbc.Col([ ], width = 1)            
                                ])
                       , dbc.Row([html.P(' ')]) 
                     ])
LayoutExp = html.Div([ html.P('Report') ])
LayoutAud = html.Div([ html.P('Audit') ])

app.layout = html.Div([ 
                        html.Div([ 
                            #########HEADER AREE DELLA WEBAPP
                            dbc.Row([html.P(' ')])
                            , dbc.Row([  dbc.Col([  ], width = 1)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/home.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='homePage', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/search.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='searchPage', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/risk.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='RiskManagement', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/expSearch.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='ExploratoryAnalysis', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/audit.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='Audit', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([  ], width = 1)
                                     ])
                               , dbc.Row([html.P(' ')])
                                ], style = {'background-color': 'lightgray'})
                        
                        ,html.Div( children = [html.Br()])
                        
                        #########BODY DELLA WEBAPP
                        , html.Div( id = 'body'
                                    , children = [Layouthome], style = {'background-color': 'lightgray'})
                        
                        ])

############################################################################################################
######################################################CALLBACKS
@app.callback(
    [dash.dependencies.Output('body', 'children')],
    [dash.dependencies.Input('homePage', 'n_clicks')
     , dash.dependencies.Input('searchPage', 'n_clicks')
     , dash.dependencies.Input('RiskManagement', 'n_clicks')
     , dash.dependencies.Input('ExploratoryAnalysis', 'n_clicks')
     , dash.dependencies.Input('Audit', 'n_clicks')
     ]
    , prevent_initial_call=True)
def body( n_clicks_home, n_clicks_search, n_clicks_risk, n_clicks_exp, n_clicks_aud ):
    
    ############################scelta della pagina da aprire
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if( trg == 'homePage' ): 
        rispLay = Layouthome
    elif( trg == 'searchPage' ): 
        rispLay = LayoutSearch
    elif( trg == 'RiskManagement' ): 
        rispLay = LayoutRisk
    elif( trg == 'ExploratoryAnalysis' ): 
        rispLay = LayoutExp
    elif( trg == 'Audit' ): 
        rispLay = LayoutAud
    
    return [rispLay]

############################################################################################################
######################################################STARTING SERVICE
if __name__ == '__main__':
    app.run_server(debug=True, dev_tools_hot_reload = False)














