# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 12:05:33 2021

@author: a.paladini
"""

import os
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table
from datetime import date
import pandas as pd
import dash_cytoscape as cyto

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path)

############################LAYOUT
sfondoApp = 'lightgray'
scritte = '#ffe476'
sfondoGrafo = 'grey'
button_style = {'background-color': 'white'
                , 'border-color': 'blue'}

tab_style = {'borderBottom': '1px solid ' + 'black'
             , 'padding': '6px'
             , 'fontWeight': 'bold'
             , 'backgroundColor': '#000000'
             , 'textColor': 'black'
             , 'color': 'black'}

tab_selected_style = { 'borderTop': '1px solid ' + 'black'
                        , 'borderBottom': '1px solid ' + 'black'
                        , 'backgroundColor': '#1A1D1D'
                        , 'color': 'black'
                        , 'padding': '6px'
                    }     

default_stylesheet = [{"selector": 'node',
                                'style': {
                                    'content': 'data(label)',
                                    'color': 'yellow',
                                    'text-halign':'center',
                                    'text-valign':'center',
                                    "opacity": 0.65,
                                    'z-index': 9999,
                                    'width': "10%",
                                    'height': "10%"
                                }
                            },
                            {
                                "selector": 'edge',
                                'style': {
                                    'line-color': scritte,
                                    "curve-style": "bezier",
                                    'target-arrow-shape': 'triangle',
                                    "opacity": 0.45,
                                    'z-index': 5000,
                                    'width': "2%",
                                    'height': "2%"
                                }
                            },
                            {
                                'selector': '.followerNode',
                                'style': {
                                    'background-color': '#0074D9'
                                }
                            },
                            {
                                'selector': '.followerEdge',
                                "style": {
                                    "mid-target-arrow-color": "blue",
                                    "mid-target-arrow-shape": "vee",
                                    "line-color": "#0074D9"
                                }
                            },
                            {
                                'selector': '.followingNode',
                                'style': {
                                    'background-color': '#FF4136'
                                }
                            },
                            {
                                'selector': '.followingEdge',
                                "style": {
                                    "mid-target-arrow-color": "red",
                                    "mid-target-arrow-shape": "vee",
                                    "line-color": "#FF4136",
                                }
                            },
                            {
                                "selector": '.genesis',
                                "style": {
                                    'background-color': '#B10DC9',
                                    "border-width": 2,
                                    "border-color": "purple",
                                    "border-opacity": 1,
                                    "opacity": 1,
                        
                                    "label": "data(label)",
                                    "color": "#B10DC9",
                                    "text-opacity": 1,
                                    "font-size": 12,
                                    'z-index': 9999
                                }
                            },
                            {
                                'selector': ':selected',
                                "style": {
                                    "border-width": 2,
                                    "border-color": sfondoGrafo,
                                    "border-opacity": 1,
                                    "opacity": 1,
                                    "label": "data(label)",
                                    "color": sfondoGrafo,
                                    "font-size": 12,
                                    'z-index': 9999
                                }
                            }
                        ]
    

############################DATA
df = pd.read_csv(R'data/PS_20174392719_1491204439457_log.csv')
df = df.loc[:500,:]

def structGraph(df):
    nodes = set()   
    edges = []
    for i in range(df.shape[0]): 
        edges.append( {'data': {'source': df.loc[i, 'nameOrig']
                                     , 'target': df.loc[i, 'nameDest']
                                     , 'weight': [df.loc[i,'type'], df.loc[i,'amount']]
                                     }
                               } )
        nodes.add(df.loc[i, 'nameOrig'])
        nodes.add(df.loc[i, 'nameDest'])
        
    nodes = list(nodes)
    newNodes = []
    for i in nodes:
        newNode = {'data': { 'id': i,'label': i}}
        newNodes.append(newNode)
        
    return newNodes + edges

def nodes(df):
    nodes = set() 
    for i in range(df.shape[0]): 
        nodes.add(df.loc[i, 'nameOrig'])
        nodes.add(df.loc[i, 'nameDest'])
    newNodes = []
    for i in nodes:
        newNode = { 'label': i,'value': i}
        newNodes.append(newNode)
        
    return newNodes

default_elements = structGraph(df)
nodeList = nodes(df)

############################LAYOUT
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

Layouthome = html.Div([ dbc.Row([html.P(' ')])
                        
                        ##########riga dedicata ai kpi
                        , dbc.Row([ dbc.Col([], width = 1)
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("segnalazioni ricevute: ..."
                                                        , color="primary", className="mr-1", id = 'kpiHome1', style = {'width': '100%'})
                                                            ])                                             
                                             ], width = 4 )
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Con priorità: ...", color="primary", className="mr-1", id = 'kpiHome2', style = {'width': '100%'})
                                                            ])   
                                             ], width = 2)
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Senza priorità: ...", color="primary", className="mr-1", id = 'kpiHome3', style = {'width': '100%'})
                                                            ])
                                             ], width = 2)
                                  , dbc.Col([ dbc.Container([
                                             dbc.Button("Segnalazioni speciali: ...", color="primary", className="mr-1", id = 'kpiHome4', style = {'width': '100%'})
                                                            ])
                                             ], width = 2)
                                  , dbc.Col([], width = 1)
                                 ], style = {'height': '8vh'})
                       , dbc.Row([html.P(' ')])
                       
                       ###########primo range selector
                       , dbc.Row([ dbc.Col([], width = 1)
                                   , dbc.Col([ dcc.DatePickerSingle(id='from-date1',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   , dbc.Col([ dcc.DatePickerSingle(id='to-date1',
                                                        min_date_allowed=date(1995, 8, 5),
                                                        max_date_allowed=date(2017, 9, 19),
                                                        initial_visible_month=date(2017, 8, 5),
                                                        date=date(2017, 8, 25)
                                                    )
                                    ], width = 2)
                                   
                                   , dbc.Col([], width = 1)
                                   
                                   , dbc.Col([dbc.Button("Search", color="primary", className="mr-1", id = 'homeSearch1')])
                                ])
                       , dbc.Row([html.P(' ')])
                       
                       ###########prima tabella
                       , dbc.Row([dbc.Col([], width = 1)
                                  , dbc.Col([dash_table.DataTable(id = 'table1home'
                                                  , columns = [  {"id": 'Mittente', "name":"Mittente"}
                                                               , {"id": 'Destinatario', "name":"Destinatario"}
                                                               , {"id": 'Transazione', "name":"Transazione"}
                                                               , {"id": 'Importo', "name":"Importo"}
                                                               , {"id": 'Rischio', "name":"Rischio"}]
                                                 # , data = df.to_dict('rows')
                                                  , page_size= 10
                                                  , sort_action='native'
                                                  , filter_action="native"
                                                  )
                                             ], width = 10)
                                  , dbc.Col([], width = 1)
                               ])
                       , dbc.Row([html.P(' ')])
                       
                       ])
                        
LayoutSearch = html.Div([ dbc.Row([html.P(' ')])
                        ##########riga della prima schermata a tabs
                        , dbc.Row([ dbc.Col([], width = 1)
                                    , dbc.Col([
                                        dcc.Tabs(id='tabs'
                                                   , parent_className='custom-tabs'
                                                   , className='custom-tabs-container'
                                                   , children=[
                                                                dcc.Tab(label='Transazioni', children=[html.Div(id='SNA'
                                                                                                                , children = [dash_table.DataTable(id = 'table1home'
                                                                                                                              , columns = [ {'id': 'step'            , 'name': 'step'         }, 
                                                                                                                                            {'id': 'type'            , 'name': 'type'          },
                                                                                                                                            {'id': 'amount'          , 'name': 'amount'        },
                                                                                                                                            {'id': 'nameOrig'        , 'name': 'nameOrig'      },
                                                                                                                                            {'id': 'oldbalanceOrg'   , 'name': 'oldbalanceOrg' },
                                                                                                                                            {'id': 'newbalanceOrig'  , 'name': 'newbalanceOrig'},
                                                                                                                                            {'id': 'nameDest'        , 'name': 'nameDest'      },
                                                                                                                                            {'id': 'oldbalanceDest'  , 'name': 'oldbalanceDest'},
                                                                                                                                            {'id': 'newbalanceDest'  , 'name': 'newbalanceDest'},
                                                                                                                                            {'id': 'isFraud'         , 'name': 'isFraud'       },
                                                                                                                                            {'id': 'isFlaggedFraud'  , 'name': 'isFlaggedFraud'}]
                                                                                                                              , data = df.to_dict('rows')
                                                                                                                              , page_size= 10
                                                                                                                              , sort_action='native'
                                                                                                                              , filter_action="native"
                                                                                                                              )
                                                                                                                               ])
                                                                        ]) 
                                                                , dcc.Tab(label='Conto corrente', children=[html.Div(id='STA', children = [dash_table.DataTable(id = 'table1home'
                                                                                                                                                                  , columns = [ {'id': 'id utente'       , 'name': 'id utente'     }, 
                                                                                                                                                                                {'id': 'nome'            , 'name': 'nome'          },
                                                                                                                                                                                {'id': 'cognome'            , 'name': 'cognome'          },
                                                                                                                                                                                {'id': 'codice fiscale'  , 'name': 'codice fiscale'},
                                                                                                                                                                                ]
                                                                                                                                                                  , data = [{ 'utente' : 'C1231006815',     
                                                                                                                                                                               'nome' :   'paolo',
                                                                                                                                                                               'cognome': 'rossi',   
                                                                                                                                                                               'codice fiscale': '11111'}]
                                                                                                                                                                  , page_size= 10
                                                                                                                                                                  , sort_action='native'
                                                                                                                                                                  , filter_action="native"
                                                                                                                                                                  )
                                                                    ])
                                                                        ])
                                                                , dcc.Tab(label='Dettaglio transazione', children=[html.Div(id='COM', children = [])
                                                                        ])
                                                                ])
                                              ])
                                    , dbc.Col([], width = 1)
                                ])
                        , dbc.Row([html.P(' ')])    
                        , dbc.Row([html.P(' ')])       
                       ])
LayoutRisk = html.Div([  dbc.Row([html.P(' ')])
                        ##########filtri su due colonne
                        , dbc.Row([  dbc.Col([], width = 1)
                                    , dbc.Col([  dcc.Dropdown(  id='dropdown-layout'
                                                                  , value='random'
                                                                  , options= [  {'label':'random', 'value':'random'}
                                                                                , {'label':'grid', 'value':'grid'}
                                                                                , {'label':'circle', 'value':'circle'}
                                                                                , {'label':'concentric', 'value':'concentric'}
                                                                                , {'label':'breadthfirst', 'value':'breadthfirst'}
                                                                                , {'label':'cose', 'value':'cose'}
                                                                             ]
                                                                             , style = {'width': '100%'
                                                                             , 'backgroundColor': '#F2F2F2'} ) 
                                                , html.P(' ')  
                                                , dcc.Dropdown(id='id-to-search'
                                                                        , options= nodes(df)
                                                                        , multi=True
                                                                        , style = {'width': '100%'
                                                                                    , 'backgroundColor': '#F2F2F2'})  
                                                 , html.P(' ') 
                                                 , html.Button('search', id='submit-val', n_clicks=0, style=button_style)
                                                 , html.P(' ')  
                                                 , html.P(' ') 
                                                 , html.Button('refresh', id='refresh', n_clicks=0, style=button_style)
                                                 , html.P(' ') 
                                                 , html.P(' ') 
                                                 , html.Div(id = 'userDescription', style = {'backgroundColor': 'whitesmoke'}) 
                                                 , html.P(' ') 
                                                 , html.P(' ') 
                                                 ], width = 2)
                                   , dbc.Col([cyto.Cytoscape(id='cytoscape'
                                                         , layout={'name': 'random'}
                                                         , elements=default_elements
                                                         , stylesheet=default_stylesheet
                                                         , style={
                                                             'height': '60vh',
                                                             'width': '100%',
                                                             'backgroundColor': sfondoGrafo}) 
                                              ], width = 8) 
                                   , dbc.Col([html.P(' ')], width = 1)
                                 ])
                      , dbc.Row([html.Div(id = 'outp')
                                 , html.P(' ')])    
                      , dbc.Row([ dbc.Col([ ], width = 1)
                                  , dbc.Col([ dash_table.DataTable(id = 'table1risk'
                                              , columns = [{"id": 'source', "name":"Mittente"}
                                                           , {"id": 'target', "name":"Destinatario"}
                                                           , {"id": 'type', "name":"tipo transazione"}
                                                           , {"id": 'amount', "name":"importo"}]
                                             # , data = df.to_dict('rows')
                                              , page_size= 10
                                              , sort_action='native'
                                              , filter_action="native"
                                              ) ])
                                  , dbc.Col([ ], width = 1)            
                                ])
                       , dbc.Row([html.P(' ')]) 
                     ])
LayoutExp = html.Div([ html.P('Report') ])
LayoutGest = html.Div([ html.P('Gestionale') ])

app.layout = html.Div([ 
                        html.Div([ 
                            #########HEADER AREE DELLA WEBAPP
                            dbc.Row([html.P(' ')])
                            , dbc.Row([  dbc.Col([  ], width = 1)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/home.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='homePage', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/audit.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='Gestionale', n_clicks=0 
                                                               , style = button_style)  
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/search.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='searchPage', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/risk.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='RiskManagement', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([ html.Button(html.Img( src = '/assets/report.png', style={'height':'50px', 'width':'50px'} )
                                                               , id='ExploratoryAnalysis', n_clicks=0
                                                               , style = button_style) 
                                                   ], width = 2)
                                       , dbc.Col([  ], width = 1)
                                     ])
                               , dbc.Row([html.P(' ')])
                                ], style = {'background-color': 'lightgray'})
                        
                        ,html.Div( children = [ html.Div( id = 'bodyHidden'
                                                            , children = [LayoutRisk], style = {'background-color': 'lightgray', 'display': 'none'})
                                                , html.Br()])
                        
                        #########BODY DELLA WEBAPP
                        , html.Div( id = 'body'
                                    , children = [Layouthome], style = {'background-color': 'lightgray'})
                        
                        ])

############################################################################################################
######################################################CALLBACKS
@app.callback(
    [dash.dependencies.Output('body', 'children')],
    [dash.dependencies.Input('homePage', 'n_clicks')
     , dash.dependencies.Input('searchPage', 'n_clicks')
     , dash.dependencies.Input('RiskManagement', 'n_clicks')
     , dash.dependencies.Input('ExploratoryAnalysis', 'n_clicks')
     , dash.dependencies.Input('Gestionale', 'n_clicks')
     ]
    , prevent_initial_call=True)
def body( n_clicks_home, n_clicks_search, n_clicks_risk, n_clicks_exp, n_clicks_aud ):
    
    ############################scelta della pagina da aprire
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if( trg == 'homePage' ): 
        rispLay = Layouthome
    elif( trg == 'searchPage' ): 
        rispLay = LayoutSearch
    elif( trg == 'RiskManagement' ): 
        rispLay = LayoutRisk
    elif( trg == 'ExploratoryAnalysis' ): 
        rispLay = LayoutExp
    elif( trg == 'Gestionale' ): 
        rispLay = LayoutGest
    
    return [rispLay]

######################################################TAB HOME
############################quali ispezioni visualizzare nella tabella home
@app.callback(
    dash.dependencies.Output('table1home', 'data'),
    [ dash.dependencies.Input('kpiHome1', 'n_clicks')
     , dash.dependencies.Input('kpiHome2', 'n_clicks')
     , dash.dependencies.Input('kpiHome3', 'n_clicks')
     , dash.dependencies.Input('kpiHome4', 'n_clicks')]
    , prevent_initial_call=True)
def update_tableHome(n_clicks1, n_clicks2, n_clicks3, n_clicks4):   
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if(trg == 'kpiHome1'):
        data = df[[ 'nameOrig', 'nameDest', 'type', 'amount' ]]
        data['Rischio'] = 0
        data.columns = ['Mittente','Destinatario','Transazione','Importo','Rischio']
        
    elif(trg == 'kpiHome2'):
        data = df.loc[:167, [ 'nameOrig', 'nameDest', 'type', 'amount' ]]
        data['Rischio'] = 0
        data.columns = ['Mittente','Destinatario','Transazione','Importo','Rischio']
        
    elif(trg == 'kpiHome3'):
        data = df.loc[167: 334, [ 'nameOrig', 'nameDest', 'type', 'amount' ]]
        data['Rischio'] = 0
        data.columns = ['Mittente','Destinatario','Transazione','Importo','Rischio']
        
    elif(trg == 'kpiHome4'):
        data = df.loc[334:, [ 'nameOrig', 'nameDest', 'type', 'amount' ]]
        data['Rischio'] = 0
        data.columns = ['Mittente','Destinatario','Transazione','Importo','Rischio']
        
    return data




### graph layout
@app.callback(dash.dependencies.Output('cytoscape', 'layout'),
              [dash.dependencies.Input('dropdown-layout', 'value')]
              , prevent_initial_call=True)
def update_cytoscape_layout(layout):
    return {'name': layout}

### graph edge interaction
@app.callback(dash.dependencies.Output('table1risk', 'data'),
              dash.dependencies.Input('cytoscape', 'tapEdgeData'))
def displayTapEdgeData(data):
    if data:
        app = {}
        app['source']= data['source']
        app['target']= data['target']
        app['type']= data['weight'][0]
        app['amount']= data['weight'][1]
        return [app]  
 
### graph node interaction
@app.callback(dash.dependencies.Output('userDescription', 'children'),
              dash.dependencies.Input('cytoscape', 'tapNodeData')
              , prevent_initial_call=True)
def displayNodeData(data):
    return 'info su ' + data['id'] 

###graph filter    
@app.callback( [dash.dependencies.Output('cytoscape', 'elements')]
              , [dash.dependencies.Input('submit-val', 'n_clicks')
                 , dash.dependencies.Input('refresh', 'n_clicks')]
              , [dash.dependencies.State('id-to-search', 'value')]
              , prevent_initial_call=True)
def update_filter_graph( n_clicks1, n_clicks2, user ):
    ctx = dash.callback_context
    trg = ctx.triggered[0]['prop_id'].split('.')[0]
    print(trg)
    
    if(trg == 'submit-val'):
        print(user)
        user = user[0]
        gf = df.loc[ (df.nameOrig == user) | (df.nameDest == user), : ].reset_index(drop = True)
        elements = structGraph(gf)

    elif(trg == 'refresh'):
        elements = structGraph(df)
        
    return [elements]

############################################################################################################
######################################################STARTING SERVICE
if __name__ == '__main__':
    app.run_server(debug=True, dev_tools_hot_reload = False)














